package dataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import DBConnection.ConnectionFactory;
import model.Product;
/**
 * This class is used for accessing the data base for the Product
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class ProductDAO {

	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO product (value, stock)" + "VALUES (?,?)";
	private static final String findStatementString = "SELECT * FROM product WHERE idProduct = ?";
	private static final String deleteStatementString = "DELETE FROM product WHERE idProduct = ?";
	private static final String updateStatementString = "UPDATE product SET value = ?, stock = ? WHERE idProduct = ?";
	private static final String selectStatementString = "SELECT * FROM product";
	/**
	 * Inserts a Product in the data base
	 * @param customer
	 * @return
	 */
	public static int insert(Product product){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		ResultSet rs = null;
		
		int insertedId = -1;
		
		try{
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setDouble(1, product.getValue());
			insertStatement.setInt(2, product.getStock());
			
			insertStatement.executeUpdate();
			
			rs = insertStatement.getGeneratedKeys();
			if(rs.next()){
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;		
	}
	/**
	 * Returns the Product by id 
	 * @param OrderId
	 * @return
	 */
	public static Product findById(int productId){
		
		Product toReturn = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try{
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1, productId);
			
			rs = findStatement.executeQuery();
			rs.next();
			
			Double value = rs.getDouble("value");
			int stock = rs.getInt("stock");
			
			toReturn = new Product(productId, value, stock);
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}		
		return toReturn;
	}
	/**
	 * Deletes the Product from the data base
	 * @param Product
	 */
	public static void delete(Product product){
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		
		try{
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, product.getIdProduct());
			
			deleteStatement.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * Updates the Product in the data base
	 * @param Product
	 */
	public static void update(Product product){
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		
		try{
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setDouble(1, product.getValue());
			updateStatement.setInt(2,  product.getStock());
			updateStatement.setInt(3,  product.getIdProduct());
			
			updateStatement.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * Returns the list of the Product that are in the data base
	 * @return
	 */
	public static List<Product> select(){
		List <Product> list = new ArrayList<Product>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		
		try{
			selectStatement = dbConnection.prepareStatement(selectStatementString);
			
			rs = selectStatement.executeQuery();
		
			while(rs.next()){
				Product product = new Product(rs.getInt(1), rs.getDouble(2), rs.getInt(3));
				list.add(product);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:select " + e.getMessage());
		} finally {
			ConnectionFactory.close(selectStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return list;
	}
}
