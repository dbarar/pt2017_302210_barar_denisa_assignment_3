package dataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import DBConnection.ConnectionFactory;
import model.Customer;
/**
 * This class is used for accessing the data base for the Customer
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class CustomerDAO {

	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO customer (name, address, email, phone)" + "VALUES (?,?,?,?)";
	private static final String findStatementString = "SELECT * FROM customer WHERE idCustomer = ?";
	private static final String deleteStatementString = "DELETE FROM customer WHERE idCustomer = ?";
	private static final String updateStatementString = "UPDATE customer SET name = ?, address = ?, email = ?, phone = ? WHERE idCustomer = ?";
	private static final String selectStatementString = "SELECT * FROM customer";
	
	/**
	 * Inserts a customer in the data base
	 * @param customer
	 * @return
	 */
	public static int insert(Customer customer){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		ResultSet rs = null;
		
		int insertedId = -1;
		
		try{
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, customer.getName());
			insertStatement.setString(2, customer.getAddress());
			insertStatement.setString(3, customer.getEmail());
			insertStatement.setString(4, customer.getPhone());
			
			insertStatement.executeUpdate();
			
			rs = insertStatement.getGeneratedKeys();
			if(rs.next()){
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;		
	}
	/**
	 * Returns the customer by id 
	 * @param customerId
	 * @return
	 */
	public static Customer findById(int customerId){
		
		Customer toReturn = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try{
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1, customerId);
			
			rs = findStatement.executeQuery();
			rs.next();
			
			String name = rs.getString("name");
			String address = rs.getString("address");
			String email = rs.getString("email");
			String phone = rs.getString("phone");
			
			toReturn = new Customer(customerId, name, address, email, phone);
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}		
		return toReturn;
	}
	/**
	 * Deletes the customer from the data base
	 * @param customer
	 */
	public static void delete(Customer customer){
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		
		try{
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, customer.getIdCustomer());
			
			deleteStatement.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * Updates the customer in the data base
	 * @param customer
	 */
	public static void update(Customer customer){
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		
		try{
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setString(1, customer.getName());
			updateStatement.setString(2,  customer.getAddress());
			updateStatement.setString(3,  customer.getEmail());
			updateStatement.setString(4,  customer.getPhone());
			updateStatement.setInt(5, customer.getIdCustomer());
			
			updateStatement.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * Returns the list of the customers that are in the data base
	 * @return
	 */
	public static List<Customer> select(){
		List <Customer> list = new ArrayList<Customer>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		
		try{
			selectStatement = dbConnection.prepareStatement(selectStatementString);
			
			rs = selectStatement.executeQuery();
		
			while(rs.next()){
				Customer customer = new Customer(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
				list.add(customer);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:select " + e.getMessage());
		} finally {
			ConnectionFactory.close(selectStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return list;
	}
}
