package dataAccess;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import DBConnection.ConnectionFactory;
import model.Order;
/**
 * This class is used for accessing the data base for the Order
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class OrderDAO {

	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String insertStatmentString = "INSERT INTO order_management_db.`ordert` (orderNo, date, value, idCustomer)" + "VALUES (?,?,?,?)";
	private static final String findStatmentString = "SELECT * FROM order_management_db.`ordert` where idOrder = ?";
	private static final String findByCustomerStatmentString = "SELECT * FROM order_management_db.`ordert` where idCustomer = ?";
	private static final String deleteStatementString = "DELETE FROM order_management_db.`ordert` WHERE idOrder = ?";
	private static final String updateStatementString = "UPDATE order_management_db.`ordert` SET date = ?, value = ?, orderNo = ? WHERE idOrder = ?";
	private static final String selectStatementString = "SELECT * FROM order_management_db.`ordert`";
	
	public static Date convertStringToSqlDate(String stringDate){
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		java.util.Date utilDate = new java.util.Date();
		try {
			utilDate = (Date) format.parse(stringDate);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime()); 
		return sqlDate;
	}
	
	public static String convertSqlDateToString(Date sqlDate){
		return sqlDate + "";
	}
	/**
	 * Inserts a Order in the data base
	 * @param order
	 * @return
	 */
	public static int insert(Order order){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		ResultSet rs = null;
		
		int insertedId = -1;
		
		try{
			insertStatement = dbConnection.prepareStatement(insertStatmentString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getOrderNo());
			//insertStatement.setDate(2, convertStringToSqlDate(order.getDate()));
			insertStatement.setString(2, order.getDate());
			insertStatement.setDouble(3, order.getValue());
			insertStatement.setInt(4, order.getIdCustomer());
			
			insertStatement.executeUpdate();
			
			rs = insertStatement.getGeneratedKeys();
			if(rs.next()){
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;		
	}
	/**
	 * Returns the Order by id 
	 * @param orderId
	 * @return
	 */
	public static Order findById(int orderId){
		
		Order toReturn = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try{
			findStatement = dbConnection.prepareStatement(findStatmentString);
			findStatement.setInt(1, orderId);
			
			rs = findStatement.executeQuery();
			rs.next();
			
			int orderNo = rs.getInt("orderNo");
			//Date date = rs.getDate("date");
			String date = rs.getString("date");
			Double value = rs.getDouble("value");
			int idCustomer = rs.getInt("idCustomer");
			
			//toReturn = new Order(orderId, orderNo, convertSqlDateToString(date), value, idCustomer);
			toReturn = new Order(orderId, orderNo, date, value, idCustomer);
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}		
		return toReturn;
	}
	/**
	 * Returns the Order by id Customer
	 * @param customerId
	 * @return
	 */
	public static Order findByCustomer(int customerId){
		
		Order toReturn = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try{
			findStatement = dbConnection.prepareStatement(findByCustomerStatmentString);
			findStatement.setInt(1, customerId);
			
			rs = findStatement.executeQuery();
			rs.next();
			
			int idOrder = rs.getInt("idOrder");
			int orderNo = rs.getInt("orderNo");
			//Date date = rs.getDate("date");
			String date = rs.getString("date");
			Double value = rs.getDouble("value");
			int idCustomer = rs.getInt("idCustomer");
			
			//toReturn = new Order(orderId, orderNo, convertSqlDateToString(date), value, idCustomer);
			toReturn = new Order(idOrder, orderNo, date, value, idCustomer);
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findByCustomer " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}		
		return toReturn;
	}
	/**
	 * Deletes the order from the data base
	 * @param order
	 */
	public static void delete(Order order){
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		
		try{
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, order.getIdOrder());
			
			deleteStatement.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * Updates the order in the data base
	 * @param order
	 */
	public static void update(Order order){
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		
		try{
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			//updateStatement.setDate(1, convertStringToSqlDate(order.getDate()));
			updateStatement.setString(1, order.getDate());
			updateStatement.setDouble(2,  order.getValue());
			updateStatement.setInt(3,  order.getOrderNo());
			updateStatement.setInt(4, order.getIdOrder());
			
			updateStatement.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * Returns the list of the order that are in the data base
	 * @return
	 */
	public static List<Order> select(){
		List <Order> list = new ArrayList<Order>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		
		try{
			selectStatement = dbConnection.prepareStatement(selectStatementString);
			
			rs = selectStatement.executeQuery();
		
			while(rs.next()){
				Order order = new Order(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getDouble(4), rs.getInt(5));
				list.add(order);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:select " + e.getMessage());
		} finally {
			ConnectionFactory.close(selectStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return list;
	}
	
}
