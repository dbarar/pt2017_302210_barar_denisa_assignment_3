package dataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import DBConnection.ConnectionFactory;
import model.OrderProduct;
/**
 * This class is used for accessing the data base for the Order
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class Order_ProductDAO {

	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO order_product (quantity, idOrder, idProduct)" + "VALUES (?,?,?)";
	private static final String findStatementString = "SELECT * FROM order_product WHERE idOrderProduct = ?";
	private static final String deleteStatementString = "DELETE FROM order_product WHERE idOrderProduct = ?";
	private static final String updateStatementString = "UPDATE order_product SET qantity = ? WHERE idOrderProduct = ?"; 
	private static final String selectStatementString = "SELECT * FROM order_product";
	/**
	 * Inserts a orderProduct in the data base
	 * @param order
	 * @return
	 */
	public static int insert(OrderProduct order){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;
		ResultSet rs = null;
		
		int insertedId = -1;
		
		try{
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getQuantity());
			insertStatement.setInt(2, order.getidOrder());
			insertStatement.setInt(3, order.getidProduct());
			
			insertStatement.executeUpdate();
			
			rs = insertStatement.getGeneratedKeys();
			if(rs.next()){
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderProductDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;		
	}
	/**
	 * Returns the orderProduct by id 
	 * @param OrderId
	 * @return
	 */
	public static OrderProduct findById(int orderId){
		
		OrderProduct toReturn = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		
		try{
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1, orderId);
			
			rs = findStatement.executeQuery();
			rs.next();
			
			int quantity = rs.getInt("quantity");
			int idOrder = rs.getInt("idOrder");
			int idProduct = rs.getInt("idProduct");
			
			toReturn = new OrderProduct(orderId, quantity, idOrder, idProduct);
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}		
		return toReturn;
	}
	/**
	 * Deletes the orderProduct from the data base
	 * @param orderProduct
	 */
	public static void delete(OrderProduct orderProduct){
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		
		try{
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, orderProduct.getIdOrderProduct());
			
			deleteStatement.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * Updates the orderProduct in the data base
	 * @param orderProduct
	 */
	public static void update(OrderProduct orderProduct){
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		
		try{
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setInt(1, orderProduct.getQuantity());
			updateStatement.setInt(2,  orderProduct.getIdOrderProduct());

			updateStatement.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderProductDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	/**
	 * Returns the list of the orderProduct that are in the data base
	 * @return
	 */
	public static List<OrderProduct> select(){
		List <OrderProduct> list = new ArrayList<OrderProduct>();
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement selectStatement = null;
		ResultSet rs = null;
		
		try{
			selectStatement = dbConnection.prepareStatement(selectStatementString);
			
			rs = selectStatement.executeQuery();
		
			while(rs.next()){
				OrderProduct orderProduct = new OrderProduct(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4));
				list.add(orderProduct);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:select " + e.getMessage());
		} finally {
			ConnectionFactory.close(selectStatement);
			ConnectionFactory.close(dbConnection);
		}
		
		return list;
	}
}
