package businessLogic.validators;

import model.Customer;
/**
 * This class implements the validator for the phone by matching the string with regex patterns
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class PhoneValidator implements Validator<Customer> {

	/**
	 * validates the phone number
	 */
	public void validate(Customer t) {
		String phoneNo = t.getPhone();
		boolean flag = false;
		//validate phone numbers of format "1234567890"
		if (phoneNo.matches("\\d{10}")) 
			flag = true;
		//validating phone number with -, . or spaces
		else if(phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) 
			flag = true;
		//validating phone number with extension length from 3 to 5
		else if(phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) 
			flag = true;
		//validating phone number where area code is in braces ()
		else if(phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) 
			flag = true;
		
		if(!flag){
			throw new IllegalArgumentException("Phone number is not valid!");
		}
				
	}
}
