package businessLogic.validators;

import model.Order;
import model.OrderProduct;
import model.Product;
/**
 * This class implements the validator for the value in order not to let the value to be negative
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class ValueValidator<T> implements Validator<T>{

	/**
	 * validate method
	 */
	public void validate(T t) {
		// TODO Auto-generated method stub
		if(t.getClass() == Product.class){
			if(((Product) t).getValue() < 0){
				throw new IllegalArgumentException("The Product value is below zero!");
			}
			if(((Product) t).getStock() < 0){
				throw new IllegalArgumentException("The Product stock is below zero!");
			}
		}else if(t.getClass() == Order.class){
			if(((Order) t).getValue() < 0){
				throw new IllegalArgumentException("The Order value is below zero!");
			}
		}else if(t.getClass() == OrderProduct.class){
			if(((OrderProduct) t).getQuantity() < 0){
				throw new IllegalArgumentException("The OrderProduct quantity is below zero!");
			}
		}
	}

}
