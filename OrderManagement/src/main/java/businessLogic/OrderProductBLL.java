package businessLogic;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import businessLogic.validators.Validator;
import businessLogic.validators.ValueValidator;
import dataAccess.Order_ProductDAO;
import model.OrderProduct;
/**
 * This class implements the model the logic used for processing the customer
 * makes the connection between the data access class and the graphical interface
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class OrderProductBLL {

private List<Validator<OrderProduct>> validators; 
	/**
	 * This constructs an object that will be used for implementing the logic of the Customer
	 * This assigns the specified validators  
	 */
	public OrderProductBLL(){
		validators = new ArrayList<Validator<OrderProduct>>();
		validators.add(new ValueValidator<OrderProduct>());
	}
	/**
	 * This returns the orderProduct by id
	 * @param id
	 * @return
	 */
	public OrderProduct findOrderById(int id){
		OrderProduct order = Order_ProductDAO.findById(id);
		if(order == null)
			throw new NoSuchElementException("The order with id =" + id + " was not found!");
		return order;
	}
	/**
	 * This inserts a orderProduct
	 * @param orderProduct
	 * @return
	 */
	public int insertOrderProduct(OrderProduct order){
		for(Validator<OrderProduct> v: validators){
			v.validate(order);
		}
		return Order_ProductDAO.insert(order);
	}
	/**
	 * This deletes a orderProduct
	 * @param orderProduct
	 */
	public void deleteOrderProduct(OrderProduct order){
		Order_ProductDAO.delete(order);
	}
	/**
	 * This deletes a orderProduct
	 * @param id
	 */
	public void deleteOrderProduct(int id){
		OrderProduct order = Order_ProductDAO.findById(id);
		if(order == null){
			throw new NoSuchElementException("The customer with id =" + id + " was not found, it cannot be deleted!");
		}
		this.deleteOrderProduct(order);
	}
	/**
	 * This updates a orderProduct
	 * @param orderProduct
	 */
	public void updateOrderProduct(OrderProduct orderProduct){
		Order_ProductDAO.update(orderProduct);
	}
	/**
	 * This updates the orderProduct
	 * @param id
	 * @param quantity
	 */
	public void updateOrderProduct(int id, int quantity){
		OrderProduct orderProduct = Order_ProductDAO.findById(id);
		orderProduct.setQuantity(quantity);
		Order_ProductDAO.update(orderProduct);
	}
	/**
	 * This returns a list with all the orderProduct that are in the data base
	 * @return
	 */
	public List<OrderProduct> selectOrderProduct(){
		List<OrderProduct> list = new ArrayList<OrderProduct>();
		list = Order_ProductDAO.select();
		return list;
	}
}
