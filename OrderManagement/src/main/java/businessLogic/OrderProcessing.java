package businessLogic;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import dataAccess.CustomerDAO;
import dataAccess.OrderDAO;
import dataAccess.Order_ProductDAO;
import dataAccess.ProductDAO;
import model.*;
/**
 * This class implements the logic for proccesing the order
 * @author denisa
 *
 */
public class OrderProcessing {

	/**
	 * 
	 */
	
	private static Customer customer;
	private static Product product;
	private static Order order;
	private static OrderProduct orderProduct;
	private static int quantity;
	private static int idCustomer;
	private static int idProduct;
	private static String date;
	
	/**
	 * Constructs an object for order processing
	 * @param quantity
	 * @param idCustomer
	 * @param idProduct
	 * @param date
	 */
	public OrderProcessing(int quantity, int idCustomer, int idProduct, String date){
		OrderProcessing.quantity = quantity;
		OrderProcessing.idCustomer = idCustomer;
		OrderProcessing.idProduct = idProduct;
		OrderProcessing.date = date;
	}
	
	/**
	 * makes the Order
	 * @return 1 if the customer is null
	 * @return 2 if the product is null
	 * @return -1 if the stock is insufficient
	 * @return 0 if success
	 */
	public static int makeOrder(){
		customer = CustomerDAO.findById(idCustomer);
		product = ProductDAO.findById(idProduct);
		
		if(customer == null){			
			return 1;
		}
		if(product == null){
			return 2;
		}
		
		if(product.getStock() < quantity){
			return -1;
		}
		
		order = OrderDAO.findByCustomer(idCustomer);
		
		double value;
		if(order == null){
			value = quantity * product.getValue();
			order = new Order(idCustomer, date, value, idCustomer);
			order.setIdOrder(OrderDAO.insert(order));
		}else{
			int id = order.getIdOrder();
			value = order.getValue() + quantity * product.getValue();
			order = new Order(id, idCustomer, date, value, idCustomer);
			OrderDAO.update(order);
		}
		
		orderProduct = new OrderProduct(quantity, order.getIdOrder(), product.getIdProduct());
		Order_ProductDAO.insert(orderProduct);
		product.setStock(product.getStock() - 1);
		ProductDAO.update(product);
		
		String fileName = new String("order" + orderProduct.getidOrder() + customer.getIdCustomer()) + ".txt";
		String toWrite = new String("ORDER MADE FOR CUSTOMER: " + customer.toString() + " \nPRODUCT: " + product.toString() + " \nORDER: " + order.toString() + " \nORDER PRODUCT " + orderProduct.toString());
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
	              new FileOutputStream(fileName), "utf-8"))) {
			writer.write(toWrite);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}
