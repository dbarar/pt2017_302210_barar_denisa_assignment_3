package businessLogic;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import businessLogic.validators.Validator;
import businessLogic.validators.ValueValidator;
import dataAccess.OrderDAO;
import model.Order;

/**
 * This class implements the model the logic used for processing the order
 * makes the connection between the data access class and the graphical interface
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */

public class OrderBLL {

	private List<Validator<Order>> validators; 
	
	/**
	 * This constructs an object that will be used for implementing the logic of the Customer
	 * This assigns the specified validators  
	 */
	public OrderBLL(){
		validators = new ArrayList<Validator<Order>>();
		validators.add(new ValueValidator<Order>());
	}
	
	/**
	 * This returns the Order by id
	 * @param id
	 * @return
	 */
	public Order findOrderById(int id){
		Order order = OrderDAO.findById(id);
		if(order == null)
			throw new NoSuchElementException("The order with id =" + id + " was not found!");
		return order;
	}
	
	/**
	 * This inserts a Order
	 * @param Order
	 * @return
	 */
	public int insertOrder(Order order){
		for(Validator<Order> v: validators){
			v.validate(order);
		}
		return OrderDAO.insert(order);
	}
	
	/**
	 * This deletes a Order
	 * @param Order
	 */
	public void deleteOrder(Order order){
		OrderDAO.delete(order);
	}
	
	/**
	 * This deletes a Order
	 * @param id
	 */
	public void deleteOrder(int id){
		Order order = OrderDAO.findById(id);
		if(order == null){
			throw new NoSuchElementException("The customer with id =" + id + " was not found, it cannot be deleted!");
		}
		this.deleteOrder(order);
	}
	
	/**
	 * This updates a Order
	 * @param Order
	 */
	public void updateOrder(Order order){
		OrderDAO.update(order);
	}
	
	/**
	 * This updates the Order
	 * @param id
	 * @param date
	 */
	public void updateOrderDate(int id, String date){
		Order order = OrderDAO.findById(id);
		order.setDate(date);
		OrderDAO.update(order);
	}
	
	/**
	 * This updates the Order
	 * @param id
	 * @param value
	 */
	public void updateOrderValue(int id, double value){
		Order order = OrderDAO.findById(id);
		order.setValue(value);
		OrderDAO.update(order);
	}
	
	/**
	 * This returns a list with all the Order that are in the data base
	 * @return
	 */
	public List<Order> selectOrder(){
		List<Order> list = new ArrayList<Order>();
		list = OrderDAO.select();
		return list;
	}
}
