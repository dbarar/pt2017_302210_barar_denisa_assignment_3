package businessLogic;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import businessLogic.validators.EmailValidator;
import businessLogic.validators.PhoneValidator;
import businessLogic.validators.Validator;
import dataAccess.CustomerDAO;
import model.Customer;
/**
 * This class implements the model the logic used for processing the customer
 * makes the connection between the data access class and the graphical interface
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class CustomerBLL {

	private List<Validator<Customer>> validators;
	
	/**
	 * This constructs an object that will be used for implementing the logic of the Customer
	 * This assigns the specified validators  
	 */
	public CustomerBLL(){
		validators = new ArrayList<Validator<Customer>>();
		validators.add(new EmailValidator());
		validators.add(new PhoneValidator());
	}
	
	/**
	 * This returns the customer by id
	 * @param id
	 * @return
	 */
	public Customer findCustomerById(int id){
		Customer customer = CustomerDAO.findById(id);
		if(customer == null){
			throw new NoSuchElementException("The customer with id =" + id + " was not found!");
		}
		return customer;
	}
	
	/**
	 * This inserts a customer
	 * @param customer
	 * @return
	 */
	public int insertCustomer(Customer customer){
		for (Validator<Customer> v : validators) {
			v.validate(customer);
		}
		return CustomerDAO.insert(customer);
	}
	
	/**
	 * This deletes a Customer
	 * @param customer
	 */
	public void deleteCustomer(Customer customer){
		CustomerDAO.delete(customer);
	}
	
	/**
	 * This deletes a Customer
	 * @param id
	 */
	public void deleteCustomer(int id){
		Customer customer = CustomerDAO.findById(id);
		if(customer == null){
			throw new NoSuchElementException("The customer with id =" + id + " was not found, it cannot be deleted!");
		}
		this.deleteCustomer(customer);
	}
	
	/**
	 * This updates a Customer
	 * @param customer
	 */
	public void updateCustomer(Customer customer){
		CustomerDAO.update(customer);
	}
	
	/**
	 * This updates the customer
	 * @param id
	 * @param name
	 */
	public void updateCustomerAddress(int id, String name){
		Customer customer = CustomerDAO.findById(id);
		customer.setName(name);
		this.updateCustomer(customer);
	}
	
	/**
	 * This updates the customer
	 * @param id
	 * @param name
	 */
	public void updateCustomerEmail(int id, String name){
		Customer customer = CustomerDAO.findById(id);
		customer.setEmail(name);
		this.updateCustomer(customer);
	}
	
	/**
	 * This updates the customer
	 * @param id
	 * @param phone
	 */
	public void updateCustomerPhone(int id, String phone){
		Customer customer = CustomerDAO.findById(id);
		customer.setPhone(phone);
		this.updateCustomer(customer);
	}
	
	/**
	 * This returns a list with all the Customers that are in the data base
	 * @return
	 */
	public List<Customer> selectCustomer(){
		List<Customer> list = new ArrayList<Customer>();
		list = CustomerDAO.select();
		return list;
	}
}
