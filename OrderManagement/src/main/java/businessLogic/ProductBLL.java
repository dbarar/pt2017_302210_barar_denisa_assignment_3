package businessLogic;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import businessLogic.validators.Validator;
import businessLogic.validators.ValueValidator;
import dataAccess.ProductDAO;
import model.Product;
/**
 * This class implements the model the logic used for processing the customer
 * makes the connection between the data access class and the graphical interface
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class ProductBLL {

	private List<Validator<Product>> validators; 
	/**
	 * This constructs an object that will be used for implementing the logic of the Customer
	 * This assigns the specified validators  
	 */
	public ProductBLL(){
		validators = new ArrayList<Validator<Product>>();
		validators.add(new ValueValidator<Product>());
	}
	/**
	 * This returns the product by id
	 * @param id
	 * @return
	 */
	public Product findProductById(int id){
		Product product = ProductDAO.findById(id);
		if(product == null)
			throw new NoSuchElementException("The order with id =" + id + " was not found!");
		return product;
	}
	/**
	 * This inserts a product
	 * @param product
	 * @return
	 */
	public int insertProduct(Product product){
		for(Validator<Product> v: validators){
			v.validate(product);
		}
		return ProductDAO.insert(product);
	}
	/**
	 * This deletes a product
	 * @param product
	 */
	public void deleteProduct(Product product){
		ProductDAO.delete(product);
	}
	/**
	 * This deletes a product
	 * @param id
	 */
	public void deleteProduct(int id){
		Product product = ProductDAO.findById(id);
		if(product == null){
			throw new NoSuchElementException("The customer with id =" + id + " was not found, it cannot be deleted!");
		}
		this.deleteProduct(product);
	}
	/**
	 * This updates a product
	 * @param product
	 */
	public void updateProduct(Product product){
		ProductDAO.update(product);
	}
	/**
	 * This updates the product
	 * @param id
	 * @param value
	 */
	public void updateProductValue(int id, double value){
		Product product = ProductDAO.findById(id);
		product.setValue(value);
		ProductDAO.update(product);
	}
	/**
	 * This updates the product
	 * @param id
	 * @param stock
	 */
	public void updateProductStock(int id, int stock){
		Product product = ProductDAO.findById(id);
		product.setStock(stock);
		ProductDAO.update(product);
	}
	/**
	 * This returns a list with all the product that are in the data base
	 * @return
	 */
	public List<Product> selectProduct(){
		List<Product> list = new ArrayList<Product>();
		list = ProductDAO.select();
		return list;
	}
}
