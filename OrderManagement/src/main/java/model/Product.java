package model;

import java.io.Serializable;

/**
 * This POJO class implements the model of the Customer that can make an Order after buying Products
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2112340298600386287L;
	
	private int idProduct;
	private double value;
	private int stock;
	
	/**
	 * This constructor creates a Product with a specified idProduct, a value and a stock
	 * @param idProduct
	 * @param value
	 * @param stock
	 */
	public Product(int idProduct, double value, int stock){
		this.idProduct = idProduct;
		this.value = value;
		this.stock = stock;
	}
	
	/**
	 * This constructor creates a Product with a specified value and a stock
	 * @param value
	 * @param stock
	 */
	public Product(double value, int stock){
		this(0, value, stock);
	}
	
	/**
	 * This constructor creates a Product with a no specifications
	 */
	public Product(){
		this(0, 0.0, 0);
	}
	
	/**
	 * This returns the idProduct of a product
	 * @return
	 */
	public int getIdProduct() {
		return idProduct;
	}
	
	/**
	 * This sets the idProduct of a product
	 * @param idProduct
	 */
	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	
	/**
	 * This returns the value of a product 
	 * @return
	 */
	public double getValue() {
		return value;
	}
	
	/**
	 * This sets the value of a product
	 * @param value
	 */
	public void setValue(double value) {
		this.value = value;
	}
	
	/**
	 * This returns the stock of a product
	 * @return
	 */
	public int getStock() {
		return stock;
	}
	
	/**
	 * This sets the stock of a product
	 * @param stock
	 */
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	/**
	 * This returns a string representation of the object
	 */
	public String toString(){
		return "idProduct: " + idProduct + " value: " + value + " stock: " + stock + "\n";
	}
}
