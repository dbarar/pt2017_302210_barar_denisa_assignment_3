package model;

import java.io.Serializable;

/**
 * This POJO class implements the model of the Order that contains dates of the serial number of the order, about the date, about the value and a reference to the Customer who has made the order
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class Order implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int idOrder;
	private int orderNo;
	private String date;
	private double value;
	private int idCustomer;
	
	/**
	 * This constructs an Order with all the specifications
	 * @param idOrder identification of the Order 
	 * @param orderNo order serial number
	 * @param date when the Order was made
	 * @param value the total value of the Order
	 * @param idCustomer the id of the Customer who has ordered
	 */
	public Order(int idOrder, int orderNo, String date, Double value, int idCustomer){
		this.idOrder = idOrder;
		this.orderNo = orderNo;
		this.date = date;
		this.value = value;
		this.idCustomer = idCustomer;
	}
	
	/**
	 * This constructs an Order with a specified order number, a date, a value and the id of the Customer
	 * @param orderNo order serial number
	 * @param date when the Order was made
	 * @param value the total value of the Order
	 * @param idCustomer the id of the Customer who has ordered
	 */
	public Order(int orderNo, String date, Double value, int idCustomer){
		this(0, orderNo, date, value, idCustomer);
	}
	
	/**
	 * This constructs an Order with no specification
	 */
	public Order(){
		this(0, 0, null, 0.0, 0);
	}
	
	/**
	 * This returns the identification of the order
	 * @return idOrder
	 */
	public int getIdOrder() {
		return idOrder;
	}
	
	/**
	 * This sets the id order
	 * @param idOrder
	 */
	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}
	
	/**
	 * This returns the number of the order
	 * @return orderNo
	 */
	public int getOrderNo() {
		return orderNo;
	}
	
	/**
	 * This sets the order number of the order
	 * @param orderNo
	 */
	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}
	
	/**
	 * This returns the date of the order
	 * @return date
	 */
	public String getDate() {
		return date;
	}
	
	/**
	 * This sets the date of the order
	 * @param date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * This returns the value of the order
	 * @return value
	 */
	public double getValue() {
		return value;
	}
	
	/**
	 * This sets the value of the order
	 * @param value
	 */
	public void setValue(double value) {
		this.value = value;
	}
	
	/**
	 * This returns the id of the customer who has ordered
	 * @return
	 */
	public int getIdCustomer() {
		return idCustomer;
	}
	
	/**
	 * This sets the id of the customer who has ordered
	 * @param idCustomer
	 */
	public void setIdCustomer(int idCustomer) {
		this.idCustomer = idCustomer;
	}
	
	/**
	 * This returns a string representation of the object
	 */
	public String toString(){
		return "idOrder: " + idOrder + " orderNo: " + orderNo + " date: " + date + " value: " + value + " idCustomer: " + idCustomer + "\n";
	}
}
