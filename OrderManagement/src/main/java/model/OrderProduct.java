package model;

import java.io.Serializable;
/**
 * This POJO class implements the model of the OrderProduct that makes the connection between the Product and the Order specifying the quantity of the product that was ordered
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class OrderProduct implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idOrderProduct;
	private int quantity;
	private int idOrder;
	private int idProduct;
	
	/**
	 * This constructs on OrderProduct by specifying all the attributes
	 * @param idOrderProduct the id
	 * @param quantity the quantity of the product that appears on the order
	 * @param idOrder the id of the order
	 * @param idProduct the id of the product
	 */
	public OrderProduct(int idOrderProduct, int quantity, int idOrder, int idProduct){
		this.idOrderProduct = idOrderProduct;
		this.quantity = quantity;
		this.idOrder = idOrder;
		this.idProduct = idProduct;
	}
	
	/**
	 * This constructs on OrderProduct with a specified quantity, idOrder and idProduct
	 * @param quantity the quantity of the product that appears on the order
	 * @param idOrder the id of the order
	 * @param idProduct the id of the product
	 */
	public OrderProduct(int quantity, int idOrder, int idProduct){
		this(0, quantity, idOrder, idProduct);
	}
	
	/**
	 * This constructs an Order Product with no specifications
	 */
	public OrderProduct(){
		this(0, 0, 0, 0);
	}
	
	/**
	 * This returns the id of the OrderProduct object
	 * @return
	 */
	public int getIdOrderProduct() {
		return idOrderProduct;
	}
	
	/**
	 * This sets the id of the OrderProduct object
	 * @param idOrderProduct
	 */
	public void setIdOrderProduct(int idOrderProduct) {
		this.idOrderProduct = idOrderProduct;
	}
	
	/**
	 * This returns the quantity of the OrderProduct object 
	 * @return
	 */
	public int getQuantity() {
		return quantity;
	}
	
	/**
	 * This sets the quantity of the OrderProduct object 
	 * @param quantity
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	/**
	 * This returns the id of the Order
	 * @return
	 */
	public int getidOrder() {
		return idOrder;
	}
	
	/**
	 * This sets the id of the Order
	 * @param idOrder
	 */
	public void setidOrder(int idOrder) {
		this.idOrder = idOrder;
	}
	
	/**
	 * This returns the id of the Product
	 * @return
	 */
	public int getidProduct() {
		return idProduct;
	}
	
	/**
	 * This sets the id of the Product
	 * @param idProduct
	 */
	public void setidProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	
	/**
	 * This returns a string representation of the object
	 */
	public String toString(){
		return "idOrderProduct: " + idOrderProduct + " quantity: " + quantity + " idOrder: " + idOrder + " idProduct: " + idProduct + "\n";
	}
}
