package model;

import java.io.Serializable;
/**
 * This POJO class implements the model of the Customer that can make an Order after buying Products
 * @author Denisa Barar
 * @version 1
 * @since April 2017
 */
public class Customer implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int idCustomer;
	private String name;
	private String address;
	private String email;
	private String phone;
	
	/**
	 * This constructs a Customer with all attributes specified
	 * @param idCustomer unique identification
	 * @param name the name of the Customer
	 * @param address the address of the Customer
	 * @param email 
	 * @param phone
	 */
	public Customer(int idCustomer, String name, String address, String email, String phone){
		this.idCustomer = idCustomer;
		this.name = name;
		this.address = address;
		this.email = email;
		this.phone = phone;
	}

	/**
	 * This constructs a Customer with a specified name, address, email and phone
	 * @param name
	 * @param address
	 * @param email
	 * @param phone
	 */
	public Customer(String name, String address, String email, String phone){
		this(0, name, address, email, phone);
	}
	
	/**
	 * This constructs a Customer with no specifications
	 */
	public Customer(){
		this(0, null, null, null, null);
	}
	
	/**
	 * This returns the id of the Customer
	 * @return idCustomer
	 */
	public int getIdCustomer() {
		return idCustomer;
	}
	
	/**
	 * This sets the id of the Customer
	 * @param idCustomer
	 */
	public void setIdCustomer(int idCustomer) {
		this.idCustomer = idCustomer;
	}
	
	/**
	 * This returns the name of the Customer
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * This sets the name of the Customer
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * This returns the address of the Customer
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	
	/**
	 * This sets the address of the Customer
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	/**
	 * This returns the email of the Customer
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * This sets the email of the Customer
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * This returns the phone of the Customer
	 * @return phone
	 */
	public String getPhone() {
		return phone;
	}
	
	/**
	 * This sets the phone of the Customer
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	/**
	 * This returns a string representation of the object
	 */
	public String toString(){
		return "name: " + name + " address: " + address + " email: " + email + " phone: " + phone + "\n"; 
	}
}
