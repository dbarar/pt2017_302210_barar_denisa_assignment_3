package DBConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * This class makes the connection to the database
 * it respects the Singleton design pattern in order to allow a unique instance of the connection
 * @author denisa
 *
 */

public class ConnectionFactory {
	private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/order_management_db";
	private static final String USER = "root";
	private static final String PASS = "root";
	
	private static ConnectionFactory singleInstance = new ConnectionFactory();
	
	/**
	 * constructor of the connection factory
	 */
	private ConnectionFactory(){
		try{
			Class.forName(DRIVER);//registers the driver
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * creates the connection
	 * @return connection
	 */
	private static Connection createConnection() {
		Connection conection = null;
		try {
			conection = DriverManager.getConnection(DBURL, USER, PASS);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.log(Level.WARNING, "An error occured while trying to connect to the database");
			e.printStackTrace();
		}
		return conection;
	}
	
	/**
	 * returns an instance of the connection to the DB
	 * @return 
	 */
	public static Connection getConnection(){
		return singleInstance.createConnection();
	}
	
	/**
	 * closes the connection
	 * @param connection
	 */
	public static void close(Connection connection){
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
			}
		}
	}
	
	/**
	 * closes the connection
	 * @param statement
	 */
	public static void close(PreparedStatement statement){
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the statement");
			}
		}
	}
	
	/**
	 * closes the connection
	 * @param resultSet
	 */
	public static void close(ResultSet resultSet){
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "An error occured while trying to close the ResultSet");
			}
		}
	}
}
