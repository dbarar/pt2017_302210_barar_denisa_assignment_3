package start;

import java.awt.EventQueue;
import java.util.logging.Logger;

import presentation.View;
/**
 * Launch the application.
 * @author denisa
 *
 */
public class MainClass {
	protected static final Logger LOGGER = Logger.getLogger(MainClass.class.getName());
	
	public static void main(String[] args) {		
		/**
		 * Launch the application.
		 */
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View frame = new View();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});	
		
	}
}
