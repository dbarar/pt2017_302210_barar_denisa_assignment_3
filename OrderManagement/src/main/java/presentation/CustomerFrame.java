package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businessLogic.CustomerBLL;
import model.Customer;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Class that makes the window for applying the operations made for the Customer
 * create
 * update
 * delete
 * find
 * @author denisa
 *
 */
public class CustomerFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2374568719275697811L;
	private JPanel contentPane;
	private JTextField idField;
	private JTextField nameField;
	private JTextField addressField;
	private JTextField emailField;
	private JTextField phoneField;

	/**
	 * Create the frame.
	 */
	public CustomerFrame() {
		setTitle("CUSTOMER");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 460, 355);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblId = new JLabel("ID");
		lblId.setBounds(40, 25, 46, 14);
		contentPane.add(lblId);
		
		JLabel lblName = new JLabel("NAME");
		lblName.setBounds(40, 57, 46, 14);
		contentPane.add(lblName);
		
		JLabel lblAddress = new JLabel("ADDRESS");
		lblAddress.setBounds(40, 90, 73, 14);
		contentPane.add(lblAddress);
		
		JLabel lblEmail = new JLabel("EMAIL");
		lblEmail.setBounds(40, 127, 46, 14);
		contentPane.add(lblEmail);
		
		JLabel lblPhone = new JLabel("PHONE");
		lblPhone.setBounds(40, 155, 46, 14);
		contentPane.add(lblPhone);
		
		idField = new JTextField();
		idField.setBounds(123, 22, 308, 20);
		contentPane.add(idField);
		idField.setColumns(10);
		
		nameField = new JTextField();
		nameField.setColumns(10);
		nameField.setBounds(123, 54, 308, 20);
		contentPane.add(nameField);
		
		addressField = new JTextField();
		addressField.setColumns(10);
		addressField.setBounds(123, 87, 308, 20);
		contentPane.add(addressField);
		
		emailField = new JTextField();
		emailField.setColumns(10);
		emailField.setBounds(123, 124, 308, 20);
		contentPane.add(emailField);
		
		phoneField = new JTextField();
		phoneField.setColumns(10);
		phoneField.setBounds(123, 152, 308, 20);
		contentPane.add(phoneField);
		
		JButton btnCreate = new JButton("CREATE");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Customer customer = new Customer(nameField.getText(), addressField.getText(), emailField.getText(), phoneField.getText());
				CustomerBLL customerBLL = new CustomerBLL();
				try{
					int id = customerBLL.insertCustomer(customer);
					idField.setText(id + "");
				}catch(Exception exc){
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}
			}
		});
		btnCreate.setBounds(10, 198, 133, 52);
		contentPane.add(btnCreate);
		
		JButton btnUpdate = new JButton("UPDATE");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Customer customer = new Customer(Integer.parseInt(idField.getText()), nameField.getText(), addressField.getText(), emailField.getText(), phoneField.getText());
				CustomerBLL customerBLL = new CustomerBLL();
				try{
					customerBLL.updateCustomer(customer);
				}catch(Exception exc){
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}				
			}
		});
		btnUpdate.setBounds(155, 198, 133, 52);
		contentPane.add(btnUpdate);
		
		JButton btnDelete = new JButton("DELETE");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomerBLL customerBLL = new CustomerBLL();
				try{
					int confirm = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete it?", null, JOptionPane.YES_NO_OPTION);
					if(confirm != 1){
						customerBLL.deleteCustomer(Integer.parseInt(idField.getText()));
						idField.setText("");
						nameField.setText("");
						addressField.setText("");
						emailField.setText("");
						phoneField.setText("");
					}						
				} catch( Exception exc ){
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}				
			}
		});
		btnDelete.setBounds(298, 198, 133, 52);
		contentPane.add(btnDelete);
		
		JButton btnFind = new JButton("FIND");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				CustomerBLL customerBLL = new CustomerBLL();
				Customer customer = new Customer();
				try{
					customer = customerBLL.findCustomerById(Integer.parseInt(idField.getText()));
					nameField.setText(customer.getName());
					addressField.setText(customer.getAddress());
					emailField.setText(customer.getEmail());
					phoneField.setText(customer.getPhone() + "");
				}catch(Exception exc){
					idField.setText("");
					nameField.setText("");
					addressField.setText("");
					emailField.setText("");
					phoneField.setText("");
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}
				
			}
		});
		btnFind.setBounds(10, 261, 133, 52);
		contentPane.add(btnFind);
		
		JButton btnMakeOrder = new JButton("MAKE ORDER");
		btnMakeOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int confirm = JOptionPane.showConfirmDialog(null, "Are you registered?", null, JOptionPane.YES_NO_OPTION);
				if(confirm != 1){
					MakeOrderFrame makeOrderFrame = new MakeOrderFrame();
					makeOrderFrame.setVisible(true);
				}else{
					CustomerFrame customerFrame = new CustomerFrame();
					customerFrame.setVisible(true);
				}
			}
		});
		btnMakeOrder.setBounds(155, 261, 133, 52);
		contentPane.add(btnMakeOrder);
		
		JButton btnNewButton = new JButton("CLEAR");
		btnNewButton.setBounds(298, 261, 133, 52);
		contentPane.add(btnNewButton);
	}

}
