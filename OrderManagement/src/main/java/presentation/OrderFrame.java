package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businessLogic.OrderBLL;
import model.Order;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
/**
 * Class that makes the window for applying the operations made for the Order
 * create
 * update
 * delete
 * find
 * @author denisa
 *
 */
public class OrderFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField idField;
	private JTextField orderNoField;
	private JTextField dateField;
	private JTextField valueField;
	private JTextField idCustomerField;

	SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

	/**
	 * Create the frame.
	 */
	public OrderFrame() {
		setTitle("ORDER");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 594, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 153, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnCreate = new JButton("CREATE");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Order order = new Order(Integer.parseInt(orderNoField.getText()), dateField.getText(), Double.parseDouble(valueField.getText()), Integer.parseInt(idCustomerField.getText()));
				OrderBLL orderBLL = new OrderBLL();
				try{
					int id = orderBLL.insertOrder(order);
					idField.setText(id + "");
				}catch(Exception exc){
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}
			}
		});
		btnCreate.setBounds(10, 198, 133, 52);
		contentPane.add(btnCreate);
		
		JButton btnUpdate = new JButton("UPDATE");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Order order = new Order(Integer.parseInt(idField.getText()), Integer.parseInt(orderNoField.getText()), dateField.getText(), Double.parseDouble(valueField.getText()), Integer.parseInt(idCustomerField.getText()));
				OrderBLL orderBLL = new OrderBLL();
				try{
					orderBLL.updateOrder(order);
				}catch(Exception exc){
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}	
			}
		});
		btnUpdate.setBounds(155, 198, 133, 52);
		contentPane.add(btnUpdate);
		
		JButton btnDelete = new JButton("DELETE");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderBLL orderBLL = new OrderBLL();
				try{
					int confirm = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete it?", null, JOptionPane.YES_NO_OPTION);
					if(confirm != 1){
						orderBLL.deleteOrder(Integer.parseInt(idField.getText()));
						idField.setText("");
						orderNoField.setText("");
						dateField.setText("");
						valueField.setText("");
						idCustomerField.setText("");
					}						
				} catch( Exception exc ){
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}	
			}
		});
		btnDelete.setBounds(298, 198, 133, 52);
		contentPane.add(btnDelete);
		
		JButton btnFind = new JButton("FIND");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderBLL orderBLL = new OrderBLL();
				Order order = new Order();
				try{
					order = orderBLL.findOrderById(Integer.parseInt(idField.getText()));
					orderNoField.setText(order.getOrderNo() + "");
					dateField.setText(order.getDate() + "");
					valueField.setText(order.getValue() + "");
					idCustomerField.setText(order.getIdCustomer() + "");
				}catch(Exception exc){
					idField.setText("");
					orderNoField.setText("");
					dateField.setText("");
					valueField.setText("");
					idCustomerField.setText("");
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}
			}
		});
		btnFind.setBounds(441, 198, 133, 52);
		contentPane.add(btnFind);
		
		JLabel lblId = new JLabel("ID");
		lblId.setBounds(20, 25, 46, 14);
		contentPane.add(lblId);
		
		JLabel lblOrderNo = new JLabel("ORDER NO");
		lblOrderNo.setBounds(20, 50, 77, 14);
		contentPane.add(lblOrderNo);
		
		JLabel lblDate = new JLabel("DATE");
		lblDate.setBounds(20, 75, 46, 14);
		contentPane.add(lblDate);
		
		JLabel lblValue = new JLabel("VALUE");
		lblValue.setBounds(20, 100, 46, 14);
		contentPane.add(lblValue);
		
		JLabel lblIdCustomer = new JLabel("ID CUSTOMER");
		lblIdCustomer.setBounds(20, 125, 94, 14);
		contentPane.add(lblIdCustomer);
		
		idField = new JTextField();
		idField.setBounds(104, 22, 470, 20);
		contentPane.add(idField);
		idField.setColumns(10);
		
		orderNoField = new JTextField();
		orderNoField.setColumns(10);
		orderNoField.setBounds(104, 47, 470, 20);
		contentPane.add(orderNoField);
		
		dateField = new JTextField();
		dateField.setColumns(10);
		dateField.setBounds(104, 72, 470, 20);
		contentPane.add(dateField);
		
		valueField = new JTextField();
		valueField.setColumns(10);
		valueField.setBounds(104, 97, 470, 20);
		contentPane.add(valueField);
		
		idCustomerField = new JTextField();
		idCustomerField.setColumns(10);
		idCustomerField.setBounds(104, 122, 470, 20);
		contentPane.add(idCustomerField);
	}
}
