package presentation;

import javax.swing.*;
import java.awt.Color;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import businessLogic.*;
import model.*;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
/**
 * Class that makes the main graphical interface
 * table
 * redirections for the other windows
 * @author denisa
 *
 */
public class View extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JPanel panel;
	private JTable table;
	private JScrollPane scrollPane;
	private JButton btnOrder;
	private JButton btnOrderproduct;
	private JLabel lblOperations;
	
	public View() {
		setTitle("ORDER MANAGEMENT");
		panel = new JPanel();
		panel.setLayout(null);
		
		JLabel titleTable = new JLabel("TABLE");
		titleTable.setFont(new Font("Tahoma", Font.PLAIN, 20));
		titleTable.setBounds(26, 40, 79, 25);
		panel.add(titleTable);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(26, 101, 430, 311);
		panel.add(scrollPane);
		
		JButton btnCustomer = new JButton("CUSTOMER");
		btnCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CustomerFrame customerFrame = new CustomerFrame();
				customerFrame.setVisible(true);
			}
		});
		btnCustomer.setBounds(503, 101, 148, 23);
		panel.add(btnCustomer);
		
		JButton btnProduct = new JButton("PRODUCT");
		btnProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProductFrame productFrame = new ProductFrame();
				productFrame.setVisible(true);
			}
		});
		btnProduct.setBounds(503, 164, 148, 23);
		panel.add(btnProduct);
		
		btnOrder = new JButton("ORDER");
		btnOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderFrame orderFrame = new OrderFrame();
				orderFrame.setVisible(true);
			}
		});
		btnOrder.setBounds(503, 233, 148, 23);
		panel.add(btnOrder);
		
		btnOrderproduct = new JButton("OrdProd");
		btnOrderproduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderProductFrame ordPrdFrame = new OrderProductFrame();
				ordPrdFrame.setVisible(true);
			}
		});
		btnOrderproduct.setBounds(503, 300, 148, 23);
		panel.add(btnOrderproduct);
		
		JButton btnMakeOrder = new JButton("MAKE ORDER");
		btnMakeOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int confirm = JOptionPane.showConfirmDialog(null, "Are you registered?", null, JOptionPane.YES_NO_OPTION);
				if(confirm != 1){
					MakeOrderFrame makeOrderFrame = new MakeOrderFrame();
					makeOrderFrame.setVisible(true);
				}else{
					CustomerFrame customerFrame = new CustomerFrame();
					customerFrame.setVisible(true);
				}
			}
		});
		btnMakeOrder.setBounds(503, 356, 148, 56);
		panel.add(btnMakeOrder);
		
		lblOperations = new JLabel("OPERATIONS");
		lblOperations.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblOperations.setBounds(516, 40, 122, 25);
		panel.add(lblOperations);
		
		final JComboBox comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = comboBox.getSelectedItem().toString();
				switch(s){
				case "CUSTOMER":
					CustomerBLL customerBLL = new CustomerBLL();
					table = makeTableForObject(customerBLL.selectCustomer());
					scrollPane.setViewportView(table);
					break;
				case "ORDER":
					OrderBLL orderBLL = new OrderBLL();
					table = makeTableForObject(orderBLL.selectOrder());
					scrollPane.setViewportView(table);
					break;
				case "PRODUCT":
					ProductBLL productBLL = new ProductBLL();
					table = makeTableForObject(productBLL.selectProduct());
					scrollPane.setViewportView(table);
					break;
				case "ORDER_PRODUCT":
					OrderProductBLL orderProductBLL = new OrderProductBLL();
					table = makeTableForObject(orderProductBLL.selectOrderProduct());
					scrollPane.setViewportView(table);
					break;
				default:
					break;
				}
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"CUSTOMER", "PRODUCT", "ORDER", "ORDER_PRODUCT"}));
		comboBox.setMaximumRowCount(4);
		comboBox.setBounds(115, 46, 163, 19);
		panel.add(comboBox);
		
		setupFrame();
	}
	
	private void setupFrame(){
		this.setContentPane(panel);
		this.setSize(725, 513);
		this.setVisible(true);
		
		panel.setBackground(new Color(100, 149, 237));
	}
	
	private <T> JTable makeTableForObject(List <T> objects){
		JTable table= new JTable();
		String[] columnNames = null;
		TableModel dataModel = null;
		Object[][] data = null;
		int length = objects.size();
		
		try{
			if(objects != null){
				
				if(objects.get(0).getClass() == Customer.class){			
					columnNames = new String[] {"ID", "NAME", "ADDRESS", "EMAIL", "PHONE"};			
					data = new Object[length][5];			
					for(int i = 0; i < length; i++){
						data[i][0] = ((Customer) objects.get(i)).getIdCustomer();
						data[i][1] = ((Customer) objects.get(i)).getName();
						data[i][2] = ((Customer) objects.get(i)).getAddress();
						data[i][3] = ((Customer) objects.get(i)).getEmail();
						data[i][4] = ((Customer) objects.get(i)).getPhone();
					}			
				}else if(objects.get(0).getClass() == Order.class){			
					columnNames = new String[] {"ID", "ORDER NO", "DATE", "VALUE", "ID CUSTOMER"};			
					data = new Object[length][5];			
					for(int i = 0; i < length; i++){
						data[i][0] = ((Order) objects.get(i)).getIdOrder();
						data[i][1] = ((Order) objects.get(i)).getOrderNo();
						data[i][2] = ((Order) objects.get(i)).getDate();
						data[i][3] = ((Order) objects.get(i)).getValue();
						data[i][4] = ((Order) objects.get(i)).getIdCustomer();
					}
				}else if(objects.get(0).getClass() == Product.class){			
					columnNames = new String[] {"ID", "VALUE", "STOCK"};			
					data = new Object[length][3];			
					for(int i = 0; i < length; i++){
						data[i][0] = ((Product) objects.get(i)).getIdProduct();
						data[i][1] = ((Product) objects.get(i)).getValue();
						data[i][2] = ((Product) objects.get(i)).getStock();
					}
				}else if(objects.get(0).getClass() == OrderProduct.class){			
					columnNames = new String[] {"ID", "QUANTITY", "ID ORDER", "ID PRODUCT"};			
					data = new Object[length][4];			
					for(int i = 0; i < length; i++){
						data[i][0] = ((OrderProduct) objects.get(i)).getIdOrderProduct();
						data[i][1] = ((OrderProduct) objects.get(i)).getQuantity();
						data[i][2] = ((OrderProduct) objects.get(i)).getidOrder();
						data[i][3] = ((OrderProduct) objects.get(i)).getidProduct();
					}
				}
				
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}		
		
		dataModel = (TableModel) new DefaultTableModel(data, columnNames);
		table.setModel(dataModel);
		return table;
	}
}
