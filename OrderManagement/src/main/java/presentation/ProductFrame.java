package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businessLogic.ProductBLL;
import model.Product;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Class that makes the window for applying the operations made for the Product
 * create
 * update
 * delete
 * find
 * @author denisa
 *
 */
public class ProductFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField idField;
	private JTextField valueField;
	private JTextField stockField;


	/**
	 * Create the frame.
	 */
	public ProductFrame() {
		setTitle("PRODUCT");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 594, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("CREATE");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Product product = new Product(Double.parseDouble(valueField.getText()), Integer.parseInt(stockField.getText()));
				ProductBLL productBLL = new ProductBLL();
				try{
					int id = productBLL.insertProduct(product);
					idField.setText(id + "");
				}catch(Exception exc){
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}
			}
		});
		button.setBounds(10, 198, 133, 52);
		contentPane.add(button);
		
		JButton button_1 = new JButton("UPDATE");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Product product = new Product(Integer.parseInt(idField.getText()), Double.parseDouble(valueField.getText()), Integer.parseInt(stockField.getText()));
				ProductBLL productBLL = new ProductBLL();
				try{
					productBLL.updateProduct(product);
				}catch(Exception exc){
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}	
			}
		});
		button_1.setBounds(155, 198, 133, 52);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("DELETE");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProductBLL productBLL = new ProductBLL();
				try{
					int confirm = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete it?", null, JOptionPane.YES_NO_OPTION);
					if(confirm != 1){
						productBLL.deleteProduct(Integer.parseInt(idField.getText()));
						idField.setText("");
						valueField.setText("");
						stockField.setText("");
					}						
				} catch( Exception exc ){
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}	
			}
		});
		button_2.setBounds(298, 198, 133, 52);
		contentPane.add(button_2);
		
		JButton btnFind = new JButton("FIND");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProductBLL productBLL = new ProductBLL();
				Product product = new Product();
				try{
					product = productBLL.findProductById(Integer.parseInt(idField.getText()));
					stockField.setText(product.getStock() + "");
					valueField.setText(product.getValue() + "");
					idField.setText(product.getIdProduct() + "");
				}catch(Exception exc){
					idField.setText("");
					valueField.setText("");
					stockField.setText("");
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}
			}
		});
		btnFind.setBounds(440, 198, 133, 52);
		contentPane.add(btnFind);
		
		JLabel lblId = new JLabel("ID");
		lblId.setBounds(22, 28, 46, 14);
		contentPane.add(lblId);
		
		JLabel lblValue = new JLabel("VALUE");
		lblValue.setBounds(22, 65, 46, 14);
		contentPane.add(lblValue);
		
		JLabel lblStock = new JLabel("STOCK");
		lblStock.setBounds(22, 109, 46, 14);
		contentPane.add(lblStock);
		
		idField = new JTextField();
		idField.setBounds(94, 25, 479, 20);
		contentPane.add(idField);
		idField.setColumns(10);
		
		valueField = new JTextField();
		valueField.setColumns(10);
		valueField.setBounds(94, 62, 479, 20);
		contentPane.add(valueField);
		
		stockField = new JTextField();
		stockField.setColumns(10);
		stockField.setBounds(94, 106, 479, 20);
		contentPane.add(stockField);
	}
}
