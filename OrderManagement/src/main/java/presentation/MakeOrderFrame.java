package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businessLogic.OrderProcessing;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * This class makes the window which implements the interface for the processing of the order
 * create
 * @author denisa
 *
 */

public class MakeOrderFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField idCField;
	private JTextField idPField;
	private JTextField qField;
	private JTextField dateField;

	
	/**
	 * Create the frame.
	 */
	public MakeOrderFrame() {
		setTitle("Order Processing");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.ORANGE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCustomer = new JLabel("CUSTOMER");
		lblCustomer.setBounds(36, 46, 86, 14);
		contentPane.add(lblCustomer);
		
		JLabel lblProduct = new JLabel("PRODUCT");
		lblProduct.setBounds(36, 102, 70, 14);
		contentPane.add(lblProduct);
		
		JLabel lblQuantity = new JLabel("QUANTITY");
		lblQuantity.setBounds(36, 154, 98, 14);
		contentPane.add(lblQuantity);
		
		JLabel lblDate = new JLabel("DATE");
		lblDate.setBounds(36, 200, 70, 14);
		contentPane.add(lblDate);
		
		idCField = new JTextField();
		idCField.setBounds(180, 43, 148, 20);
		contentPane.add(idCField);
		idCField.setColumns(10);
		
		idPField = new JTextField();
		idPField.setColumns(10);
		idPField.setBounds(180, 99, 148, 20);
		contentPane.add(idPField);
		
		qField = new JTextField();
		qField.setColumns(10);
		qField.setBounds(180, 151, 148, 20);
		contentPane.add(qField);
		
		dateField = new JTextField();
		dateField.setBounds(180, 197, 148, 20);
		contentPane.add(dateField);
		dateField.setColumns(10);
		
		JButton btnCreate = new JButton("CREATE");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderProcessing orderprocessing = new OrderProcessing(Integer.parseInt(qField.getText()), Integer.parseInt(idCField.getText()), Integer.parseInt(idPField.getText()), dateField.getText());
				int ret = orderprocessing.makeOrder();
				if(ret == 1){
					JOptionPane.showMessageDialog(null, "Nu te-ai inregistrat!");
				}else if(ret == 2){
					JOptionPane.showMessageDialog(null, "Produsul nu exista!");
				}else if(ret == -1){
					JOptionPane.showMessageDialog(null, "Produsul nu este pe stoc!");
				}
			}
		});
		btnCreate.setBounds(315, 227, 89, 23);
		contentPane.add(btnCreate);
		
		
	}
}
