package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import businessLogic.OrderProductBLL;
import model.OrderProduct;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * Class that makes the window for applying the operations made for the OrderProduct
 * create
 * update
 * delete
 * find
 * @author denisa
 *
 */
public class OrderProductFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField idField;
	private JTextField quantityField;
	private JTextField idOrderField;
	private JTextField idProductField;


	/**
	 * Create the frame.
	 */
	public OrderProductFrame() {
		setTitle("OrderProduct");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 589, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnCreate = new JButton("CREATE");
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderProduct orderProduct = new OrderProduct(Integer.parseInt(quantityField.getText()), Integer.parseInt(idOrderField.getText()), Integer.parseInt(idProductField.getText()));
				OrderProductBLL orderProductBLL = new OrderProductBLL();
				try{
					int id = orderProductBLL.insertOrderProduct(orderProduct);
					idField.setText(id + "");
				}catch(Exception exc){
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}
			}
		});
		btnCreate.setBounds(10, 198, 133, 52);
		contentPane.add(btnCreate);
		
		JButton btnUpdate = new JButton("UPDATE");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderProduct orderProduct = new OrderProduct(Integer.parseInt(idField.getText()), Integer.parseInt(quantityField.getText()), Integer.parseInt(idOrderField.getText()), Integer.parseInt(idProductField.getText()));
				OrderProductBLL orderProductBLL = new OrderProductBLL();
				try{
					orderProductBLL.updateOrderProduct(orderProduct);
				}catch(Exception exc){
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}	
			}
		});
		btnUpdate.setBounds(155, 198, 133, 52);
		contentPane.add(btnUpdate);
		
		JButton btnDelete = new JButton("DELETE");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderProductBLL orderProductBLL = new OrderProductBLL();
				try{
					int confirm = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete it?", null, JOptionPane.YES_NO_OPTION);
					if(confirm != 1){
						orderProductBLL.deleteOrderProduct(Integer.parseInt(idField.getText()));
						idField.setText("");
						quantityField.setText("");
						idOrderField.setText("");
						idProductField.setText("");
					}						
				} catch( Exception exc ){
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}	
			}
		});
		btnDelete.setBounds(298, 198, 133, 52);
		contentPane.add(btnDelete);
		
		JButton btnFind = new JButton("FIND");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderProductBLL orderProductBLL = new OrderProductBLL();
				OrderProduct orderProduct = new OrderProduct();
				try{
					orderProduct = orderProductBLL.findOrderById(Integer.parseInt(idField.getText()));
					quantityField.setText(orderProduct.getQuantity() + "");
					idOrderField.setText(orderProduct.getidOrder() + "");
					idProductField.setText(orderProduct.getidProduct() + "");
					idField.setText(orderProduct.getIdOrderProduct() + "");
				}catch(Exception exc){
					idField.setText("");
					quantityField.setText("");
					idOrderField.setText("");
					idProductField.setText("");
					JOptionPane.showMessageDialog(null, exc.getMessage());
				}
			}
		});
		btnFind.setBounds(438, 198, 133, 52);
		contentPane.add(btnFind);
		
		JLabel lblId = new JLabel("ID");
		lblId.setBounds(20, 24, 46, 14);
		contentPane.add(lblId);
		
		JLabel lblQuantity = new JLabel("QUANTITY");
		lblQuantity.setBounds(20, 68, 66, 14);
		contentPane.add(lblQuantity);
		
		JLabel lblIdOrder = new JLabel("ID ORDER");
		lblIdOrder.setBounds(20, 112, 66, 14);
		contentPane.add(lblIdOrder);
		
		JLabel lblIdProduct = new JLabel("ID PRODUCT");
		lblIdProduct.setBounds(20, 151, 85, 14);
		contentPane.add(lblIdProduct);
		
		idField = new JTextField();
		idField.setBounds(131, 21, 432, 20);
		contentPane.add(idField);
		idField.setColumns(10);
		
		quantityField = new JTextField();
		quantityField.setColumns(10);
		quantityField.setBounds(131, 65, 432, 20);
		contentPane.add(quantityField);
		
		idOrderField = new JTextField();
		idOrderField.setColumns(10);
		idOrderField.setBounds(131, 109, 432, 20);
		contentPane.add(idOrderField);
		
		idProductField = new JTextField();
		idProductField.setColumns(10);
		idProductField.setBounds(131, 148, 432, 20);
		contentPane.add(idProductField);
	}

}
